/*
 * Getting_Started_with_AVR_Turning_on_an_LED.c
 *
 * Created: 3/16/2015 1:40:48
 *  Author: Brandy
 */ 


#include <avr/io.h>

int main(void)
{
	DDRB = (0 << DDB5)|(0 << DDB4)|(0 << DDB3)|(0 << DDB2)|(1 << DDB1)|(1 << DDB0);
	//Data Direction Register of Port B is set To All Inputs Except for Pin 0 and 1 on Port B
	
    while(1)
    {
		PORTB = (1 << PORTB0);
        //TODO:: Please write your application code
		PORTB = (0 << PORTB0);
    }
}